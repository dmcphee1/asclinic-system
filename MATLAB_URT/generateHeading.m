function [angle,trajectory, rotationTime] = generateHeading(currNode,nextNode, currHeading, angSpeed, time, tDelta)
%Reteruns a trajectory element for a change in heading. Take 0deg as being
%in x axis.
x1 = currNode(1);
y1 = currNode(2);
x2 = nextNode(1);
y2 = nextNode(2);
if(x1 == x2 && y2 > y1)
    angle = (pi/2);
elseif(x1 == x2 && y2<y1)
    angle = -(pi/2);
else
    angle_mag = atan((abs(y2-y1))/(abs(x2-x1)));

    if(x2 > x1 && y2 > y1)
        angle = angle_mag;
    elseif(x2<x1 && y2 > y1)
        angle = pi - angle_mag;
    elseif(x2>x1 && y2 < y1)
        angle = -angle_mag;
    else
        angle = -pi + angle_mag;
    end
end

[rotationTime, trajectory]  = makeRotationTrajectory(currNode, currHeading, angle, angSpeed, tDelta, time);

end

