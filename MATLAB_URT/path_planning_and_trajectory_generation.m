function [planned_path , trajectory_description, adjacency_matrix, Node_CoOrds] = ...
    path_planning_and_trajectory_generation(...
        warehouse_specification ,...
        pose_initial ,...
        pose_target ...
    )
%  ---------------------------------------------------------------------  %
%% DESCRIPTION: THIS FUNCTION SHOULD IMPLEMENT PATH PLANNING AND TRAJECTORY
%% GENREATION
%
%  INPUT ARGUMENTS:
%  warehouse_specification
%                  See the function "get_warehouse_specification()" for
%                  details of how this is defined
%
%  pose_initial    The starting location and heading angle of the robot:
%                  pose_initial(1) = x_p coordinate, in meters
%                  pose_initial(2) = y_p coordinate, in meters
%                  pose_initial(3) = heading angle, in radians
%
%  pose_target     The target location and heading angle of the robot:
%                  pose_target(1) = x_p coordinate, in meters
%                  pose_target(2) = y_p coordinate, in meters
%                  pose_target(3) = heading angle, in radians
%
%  RETURN VARIABLES:
%  planned_path    The path that was planned, format is described below.
%
%  trajectory_description
%                  A description of the trajectory, format is described
%                  below.
%
%  ---------------------------------------------------------------------  %



%  ---------------------------------------------------------------------  %
%% EXTRACT THE COMPONENTS OF "pose_initial" and "pose_target"

x_p_initial  = pose_initial(1);
y_p_initial  = pose_initial(2);
phi_initial  = pose_initial(3);

x_p_target  = pose_target(1);
y_p_target  = pose_target(2);
phi_target  = pose_target(3);


%  ---------------------------------------------------------------------  %
%% REPLACE THE CODE BELOW WITH THE YOUR ALGORITHM FOR PATH PLANNING

% Similar to the warehouse specification, the planned path is:
% > Specified by an ordered set of (x,y) coordinates that are joined by
%   straight line.
% > These coordinate are put into a matrix of size Nx2, where N is the
%   number of coordinates.
% > A straight line is draw between each subsequent pair of points to
%   specify the path.

% Define a basic two section path:
planned_path = [...
        x_p_initial , y_p_initial ;...
        x_p_target , y_p_initial ;...
        x_p_target  , y_p_target ;...
    ];



%  ---------------------------------------------------------------------  %
%% REPLACE THE CODE BELOW WITH THE YOUR ALGORITHM FOR TRAJECTORY GENERATION

% IMPORTANT NOTE:
% The "trajectory_description" variable is passed to the Simulink model,
% and hence is can only be a matrix.
% Do NOT use a cell array, or struct array, or anything more "exoctic" for
% you trajectory description.
% It is advisable to use a matrix description where:
% > Each row of the matrix is a segment of the trajectory.
% > Each column respresents properties of the segment.


%% INCREASE RESOLUTION OF warehouse_specification

warehouse_x = [];
warehouse_y = [];

for i = 1:length(warehouse_specification)-1
    warehouse_x = [warehouse_x; linspace(warehouse_specification(i,1),warehouse_specification(i+1,1),3)'];
    warehouse_y = [warehouse_y; linspace(warehouse_specification(i,2),warehouse_specification(i+1,2),3)'];
end

%% Create obstacle definitions
% %Define the test obstacle
% obstacle1_specification = [...
%         1.5, 0.5 ;...
%         2, 0.5 ; ...
%         2, 1.5 ; ...
%         1.5, 1.5 ; ...
%         1.5, 0.5 ; ...
%      ];
%  % Increase resolution of test obstacle
%  test_obstacle_x = [];
%  test_obstacle_y = [];
%  
%  for i = 1:length(obstacle1_specification)-1
%     test_obstacle_x = [test_obstacle_x; linspace(obstacle1_specification(i,1),obstacle1_specification(i+1,1),2)'];
%     test_obstacle_y = [test_obstacle_y; linspace(obstacle1_specification(i,2),obstacle1_specification(i+1,2),2)'];
% end
%  

[obstacles_x_array,obstacles_y_array,obstacles_x_column,obstacles_y_column] = obstacle_points();
number_obstacles = size(obstacles_x_array);
number_obstacles = number_obstacles(2);

%% CREATED UNDIRECTED GRAPH FOR PATH PLANNING

% Find edges
[Vx,Vy] = voronoi([warehouse_x;obstacles_x_column],[warehouse_y;obstacles_y_column]);

% Find vertices
verts = voronoin([[warehouse_x;obstacles_x_column],[warehouse_y;obstacles_y_column]]);
verts_in = verts;

% Isolate edges fully inside polygon & fully outside obstacles
Vx_in = Vx;
Vy_in = Vy;
for i = length(Vx):-1:1
   if (~inpolygon(Vx(1,i),Vy(1,i),warehouse_x,warehouse_y) || ~inpolygon(Vx(2,i),Vy(2,i),warehouse_x,warehouse_y))
       Vx_in(:,i) = [];
       Vy_in(:,i) = [];
   else
       for j = 1:1:number_obstacles
          if (inpolygon(Vx(1,i),Vy(1,i),obstacles_x_array(:,j),obstacles_y_array(:,j)) || inpolygon(Vx(2,i),Vy(2,i),obstacles_x_array(:,j),obstacles_y_array(:,j))) 
              Vx_in(:,i) = [];
              Vy_in(:,i) = [];
              break
          end
       end
   end
   
end

% Isolate vertices inside polygon and outside obstacles
for i = length(verts):-1:1
   if (~inpolygon(verts(i,1),verts(i,2),warehouse_x,warehouse_y))
       verts_in(i,:) = [];
   else
       for j=1:1:number_obstacles
          if (inpolygon(verts(i,1),verts(i,2),obstacles_x_array(:,j),obstacles_y_array(:,j))) 
              verts_in(i,:) = [];
              break
          end
       end
   end
end


graphInfo = zeros(length(Vx_in),3);
% Map vertex coordinates to unique node numbers
for i = 1:length(Vx_in)
    x0 = Vx_in(1,i);
    x1 = Vx_in(2,i);
    y0 = Vy_in(1,i);
    y1 = Vy_in(2,i);
    [~, nodeA] = ismembertol([x0,y0],verts_in,0.001,'ByRows',true);
    [~, nodeB] = ismembertol([x1,y1],verts_in,0.001,'ByRows',true);
    graphInfo(i,:) = [nodeA, nodeB, sqrt((Vx_in(1,i)-Vx_in(2,i))^2 + (Vy_in(1,i)-Vy_in(2,i))^2)];
end

% Create graph
G = graph(int16(graphInfo(:,1)'), int16(graphInfo(:,2)'), graphInfo(:,3)');

% Add edge from start node to nearest node
startNode = length(verts_in)+1;
G = addnode(G,startNode);
closest = dsearchn(verts_in,[x_p_initial y_p_initial]);
dist = sqrt((x_p_initial - verts_in(closest,1))^2 + (y_p_initial - verts_in(closest,2))^2 );
G = addedge(G,startNode,closest,dist);

% Add edge from end node to nearest node
endNode = startNode + 1;
G = addnode(G,endNode);
closest = dsearchn(verts_in,[x_p_target y_p_target]);
dist = sqrt((x_p_target - verts_in(closest,1))^2 + (y_p_target - verts_in(closest,2))^2 );
G = addedge(G,endNode,closest,dist);

%% FIND SHORTEST PATH

% Get shortest path from start to end in terms of node indices
pathIndices = shortestpath(G,startNode,endNode);

% Get shortest path from start to end in terms of coordinate waypoints
verts_in = [verts_in; [x_p_initial, y_p_initial]; [x_p_target, y_p_target]];
planned_path = zeros(length(pathIndices),2);
for i = 1:length(pathIndices)
   planned_path(i,:) = verts_in(pathIndices(i),:);
end

%% TRAJECTORY GENERATION

tDelta = 0.5;
linSpeed = 0.2;
angSpeed = 0.2;
finalHead = phi_target;
initHead = phi_initial;
trajectory_description = makeTrajectory(planned_path, linSpeed, angSpeed, initHead, finalHead, tDelta);

%% PLOT THE RESULTS

 hold on
 plot(warehouse_x,warehouse_y);
 for j=1:1:number_obstacles
      plot(obstacles_x_array(:,j),obstacles_y_array(:,j))
 end
 scatter(verts_in(:,1),verts_in(:,2))

 plot(planned_path(:,1),planned_path(:,2))
%
% P = [Vx_in(1,:) Vx_in(2,:); Vy_in(1,:) Vy_in(2,:)]';
% plot(Vx_in, Vy_in);
%
plot(trajectory_description(:,1), trajectory_description(:,2))

legend('Arena','Obstacle 1','Obstacle 2','Obstacle 3','nodes','Planned Path','Trajectory Description');
xlabel('x (m)')
ylabel('y (m)')
%% Generate Adjacency Matrix
Node_Edges = table2array(G.Edges);
Start_or_End_Node_Number = [];
%Iterate through the edges and remove any with 0 distance (self-loops)

%This will occur if there is an edge from a node to itself, OR if either
%the start or end position is on top of a node. 

%If the second case is true, then we must remove that node from the list of
%verticies after the removal of redundant edges.
for i=1:length(Node_Edges)
   if (i==length(Node_Edges))
       break
   end
   if (Node_Edges(i,3)==0)
       if (Node_Edges(i,1)~= Node_Edges(i,2))
          Start_or_End_Node_Number(end+1) = max(Node_Edges(i,1:2)); 
       end
       Node_Edges(i,:) = [];
   end
end
figure
plot(graph(Node_Edges(:,1),Node_Edges(:,2)))
adjacency_matrix = full(adjacency(graph(Node_Edges(:,1),Node_Edges(:,2))));

%% Determine co-ordinates for all nodes in sequential order
%The entries in verts_in are the nodes in sequential order as specified in
%G.Edges. If we identify that a node position is duplicated and a node must 
%be removed, then this should happen here.

%The node number to be removed is identified in Start_or_End_Node_Number
Node_CoOrds = verts_in;
%Ensure that the nodes for removal are in descending order
Start_or_End_Node_Number = sort(Start_or_End_Node_Number,'descend');

%Iterate through the duplication of verts_in to remove the nodes identified
%for removal. Do this with the max value first so we don't have to deal
%with nodes changing position number.
for i=Start_or_End_Node_Number
    Node_CoOrds(i,:) = [];
end


