function trajectory = makeTrajectory(plannedPath, linSpeed, angSpeed, initHead, finalHead, tDelta)
    %plannedPath = array of nodes
    %speed in m/s
    %init = [init x, init y, init heading]
    %finalHead is the heading we expect to be at the end
    %best to choose value of tDelta that is suffieiciently small
    
    increment = tDelta*linSpeed;
    
    time = 0;
    currHeading = initHead;
    [nodes, ~] = size(plannedPath);
    trajectory = [plannedPath(1,:), currHeading, time, 1];
    
    for i = 1:(nodes-1)
        
        [currHeading, newTrajectory, rotationTime] = generateHeading(plannedPath(i,:), plannedPath(i+1,:), currHeading, angSpeed, time, tDelta);
        
        time = rotationTime;
        trajectory = [trajectory; newTrajectory];
        
        x_diff = plannedPath(i+1,1)-plannedPath(i,1);
        y_diff = plannedPath(i+1,2)-plannedPath(i,2);
        dist = sqrt(x_diff^2 + y_diff^2);
        numOfIncrements = dist/increment;
        
        x_inc = x_diff/numOfIncrements;
        y_inc = y_diff/numOfIncrements;
        
        curr_x = plannedPath(i,1);
        curr_y = plannedPath(i,2);
        
        for j = 1:numOfIncrements
            curr_x = curr_x + x_inc;
            curr_y = curr_y + y_inc;
            time = time+tDelta;
            trajectory = [trajectory;[curr_x, curr_y, currHeading, time, 0]];
        end
        
        %Allign exactly with node
        time = time+tDelta;
        trajectory = [trajectory; [plannedPath(i+1,1), plannedPath(i+1,2), currHeading, time, 0]];
        
    end
    
    %Allign with final node
    [rotationTime, newTrajectory]  = makeRotationTrajectory(plannedPath(end,:), currHeading, finalHead, angSpeed, tDelta, time);
    time = rotationTime;
    trajectory = [trajectory; newTrajectory];

    
    
end