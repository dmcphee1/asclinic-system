function [final_x_array,final_y_array,final_x_column,final_y_column] = obstacle_points()
%This function is used to define all the spatial specifications of the
%obstacles in the arena. 
%It then increases their resolution by creating more points along the
%edges.
%All obstacle x & y points are combined into 2D arrays for use in the
%path_planning_and_trajectory_generation.m function.
%Sheesh

resolution = 3;

%Define obstacle 1
obstacle_1_specification = [...
        1.5, 0.5 ;...
        2, 0.5 ; ...
        2, 1.5 ; ...
        1.5, 1.5 ; ...
        1.5, 0.5 ; ...
     ];
 % Increase resolution of obstacle
 obstacle_1_x = [];
 obstacle_1_y = [];
 
 for i = 1:length(obstacle_1_specification)-1
    obstacle_1_x = [obstacle_1_x; linspace(obstacle_1_specification(i,1),obstacle_1_specification(i+1,1),resolution)'];
    obstacle_1_y = [obstacle_1_y; linspace(obstacle_1_specification(i,2),obstacle_1_specification(i+1,2),resolution)'];
 end
 
 %Define obstacle 2
obstacle_2_specification = [...
        1, 3 ;...
        1, 2.5 ; ...
        1.5, 2.5 ; ...
        1.5, 3 ; ...
        1, 3 ; ...
     ];
 % Increase resolution of obstacle
 obstacle_2_x = [];
 obstacle_2_y = [];
 
 for i = 1:length(obstacle_2_specification)-1
    obstacle_2_x = [obstacle_2_x; linspace(obstacle_2_specification(i,1),obstacle_2_specification(i+1,1),resolution)'];
    obstacle_2_y = [obstacle_2_y; linspace(obstacle_2_specification(i,2),obstacle_2_specification(i+1,2),resolution)'];
 end

 %Define obstacle 3
 obstacle_3_specification = [...
        1.25, 2.25 ;...
        1.5, 2.25 ; ...
        1.5, 1.75 ; ...
        1.25, 1.75 ; ...
        1.25, 2.25 ; ...
     ];
 % Increase resolution of obstacle
 obstacle_3_x = [];
 obstacle_3_y = [];
 
 for i = 1:length(obstacle_2_specification)-1
    obstacle_3_x = [obstacle_3_x; linspace(obstacle_3_specification(i,1),obstacle_3_specification(i+1,1),resolution)'];
    obstacle_3_y = [obstacle_3_y; linspace(obstacle_3_specification(i,2),obstacle_3_specification(i+1,2),resolution)'];
 end
 

%Once all obstacles are defined, append into the output arrays
final_x_array = [obstacle_1_x, obstacle_2_x, obstacle_3_x];
final_y_array = [obstacle_1_y, obstacle_2_y, obstacle_3_y];
final_x_column = [obstacle_1_x; obstacle_2_x; obstacle_3_x];
final_y_column = [obstacle_1_y; obstacle_2_y; obstacle_3_y];
 
end

