clear all
T=0.02;
syms z Ki Kp

w = @(PWM) 0.241*PWM-0.225;


Km=2.21/2.185;
Tm=0.1662;


A = (T*Km/Tm)*(T*Ki*(z+1) + 2*Kp*(z-1));
B = 2*(z-1)*(z+(T/Tm)-1);

CharEqn = B+A ;

riseTime = 0.4;
%riseTime = 0.085;
omega = 1.8/riseTime;
settlingTime = 0.5;
%settlingTime = 0.17;
zeta = 4.6/(omega*settlingTime);

dclp = roots([1 2*zeta*omega omega^2])
dclpz = exp(dclp*T)

eqn1 = 0.5*(-3.7594+0.002407*Ki + 0.2407*Kp) == -(dclpz(1)+dclpz(2));
eqn2 = 0.5*(1.7594 + 0.002407*Ki - 0.2407*Kp) == dclpz(1)*dclpz(2);

 sol = solve([eqn1, eqn2], [Ki, Kp]);
 Ki_Sol = sol.Ki;
 Kp_Sol = sol.Kp;
 
 Ki_Sol = vpa(Ki_Sol,4)
 Kp_Sol = vpa(Kp_Sol,4)
 
 Ki=double(Ki_Sol);
 Kp=double(Kp_Sol);
 
%  %Test script
% t=[0.02:0.02:70*0.02];
% u=[zeros(size(1:20)) 2.2*linspace(1,1,50)];
% y=[(T*Ki_Sol/2 + Kp_Sol)*u(1)];
% 
% for i=2:1:length(u)
%     y(end+1)=(T*Ki_Sol/2 + Kp_Sol)*u(i) +(T*Ki_Sol/2-Kp_Sol)*u(i-1); 
% end

% plot(t,y)
% hold on
% plot(t,u)
% legend('controlled', 'reference')
% 
% Ki=0;
% Kp=2;

