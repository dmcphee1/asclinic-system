function [rotationTime, trajectory]  = makeRotationTrajectory(currNode, currHeading, angle, angSpeed, tDelta, time)
rotationTime = time;
diff = abs(angle - currHeading);
numOfIncrements = diff/(angSpeed*tDelta);
angle_inc = (angle - currHeading)/numOfIncrements;
trajectory = [];

 for i = 1:numOfIncrements
     rotationTime = rotationTime+tDelta;
     trajectory = [trajectory; [currNode, currHeading + i*angle_inc, rotationTime, 1]];
 end

%Allign with exact heading
rotationTime = rotationTime + tDelta;
trajectory = [trajectory; [currNode, angle, rotationTime, 1]];
end
