close all;

% Import Data to Workspace
xRef = out.xRef.Data;
yRef = out.yRef.Data;
phiRef = out.phiRef.Data;

xActual = out.xActual.Data;
yActual = out.yActual.Data;
phiActual = out.phiActual.Data;

tiledlayout(2,2);

nexttile;
plot(xRef);
hold on;
plot(xActual);
title('x response');
legend('Ref', 'Actual');

nexttile;
plot(yRef);
hold on;
plot(yActual);
title('y response');
legend('Ref', 'Actual');

nexttile;
plot(phiRef);
hold on;
plot(phiActual);
title('phi response');
legend('Ref', 'Actual');

nexttile;
plot(xRef.Data(500:end-2500),yRef.Data(500:end-2500))
hold on;
plot(xActual.Data,yActual.Data)
legend('Ref', 'Actual');
title('Overall Movement');
xlabel('x');
ylabel('y');

