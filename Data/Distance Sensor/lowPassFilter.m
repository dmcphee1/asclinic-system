function out = lowPassFilter(w0, T, in)

out = [0];

for i = 2:length(in)
    out = [out; T*out(i-1)*(1/T - w0) + T*w0*in(i-1)]; 
end

