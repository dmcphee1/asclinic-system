clear all;
close all;
infile1 = "distanceSensor.csv";
Data = csvread(infile1);
infile2 = "implemented.csv";
ImplementedData = csvread(infile2);

F = 100;
T = 1/F;
w0 = 15;

tiledlayout(3,2);

nexttile;
plot(Data);
title("unfiltered Data");

FilteredData = lowPassFilter(w0, T, Data);

nexttile;
plot(FilteredData);
title("filtered Data");

nexttile;
plot(Data);
hold on;
plot(FilteredData);
title("Comparison");
legend("raw","filter");

nexttile;
plot(ImplementedData(:,1));
title("unfiltered Measured Data");

nexttile;
plot(ImplementedData(:,2));
title("filtered measured Data");

nexttile;
plot(ImplementedData(:,1));
hold on;
plot(ImplementedData(:,2));
title("Comparison Implemented");
legend("raw","filter");



