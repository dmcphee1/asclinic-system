#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2021, The University of Melbourne, Department of Electrical and Electronic Engineering (EEE)
#
# This file is part of ASClinic-System.
#    
# See the root of the repository for license details.
#
# ----------------------------------------------------------------------------
#     _    ____   ____ _ _       _          ____            _                 
#    / \  / ___| / ___| (_)____ (_) ___    / ___| _   _ ___| |_ ___ ________  
#   / _ \ \___ \| |   | | |  _ \| |/ __|___\___ \| | | / __| __/ _ \  _   _ \ 
#  / ___ \ ___) | |___| | | | | | | (_|_____|__) | |_| \__ \ ||  __/ | | | | |
# /_/   \_\____/ \____|_|_|_| |_|_|\___|   |____/ \__, |___/\__\___|_| |_| |_|
#                                                 |___/                       
#
# DESCRIPTION:
# Template Python node with a publisher and subscriber
#
# ----------------------------------------------------------------------------





# Import the ROS-Python package
import rospy

# Import the standard message types
from std_msgs.msg import UInt32

# Import the asclinic message types
from asclinic_pkg.msg import TemplateMessage


class rotary:

    def __init__(self):

        self.template_publisher = rospy.Publisher(node_name+"/template_topic", TemplateMessage, queue_size=10)
        
        self.counter = 0
        rospy.Timer(rospy.Duration(1.0), self.timerCallbackForPublishing, oneshot=False)


        rospy.Subscriber(node_name+"/template_topic", TemplateMessage, self.templateSubscriberCallback)

    # Respond to timer callback
    def timerCallbackForPublishing(self, event):
        # Increment the counter
        self.counter += 1
        # PUBLISH A MESSAGE
        # Initialise a "TemplateMessage" struct
        # > Note that all value are set to zero by default
        template_message = TemplateMessage();
        # Set the values
        template_message.temp_bool          = True;
        template_message.temp_uint32        = self.counter;
        template_message.temp_int32         = -1;
        template_message.temp_float32       = 1.23;
        template_message.temp_float64       = -1.23;
        template_message.temp_string        = "test";
        # Append elements into the array
        template_message.temp_float64_array.append(1.1);
        template_message.temp_float64_array.append(2.2);
        template_message.temp_float64_array.append(3.3);
        # Publish the message
        self.template_publisher.publish(template_message)

    # Respond to subscriber receiving a message
    def templateSubscriberCallback(self, msg):
        # Display that a message was received
        rospy.loginfo("[TEMPLATE PY NODE] Message receieved with data = " + str(msg.temp_uint32) )

if __name__ == '__main__':
    

    # STARTING THE ROS NODE
    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    global node_name
    node_name = "template_py_node"
    rospy.init_node(node_name, anonymous=False)
    template_py_node = TemplatePyNode()
    
    rospy.loginfo(msg, "Test")
    
    # SPIN AS A SINGLE-THREADED NODE
    #
    # rospy.spin() will enter a loop, pumping callbacks.  With this version, all
    # callbacks will be called from within this thread (the main one).  ros::spin()
    # will exit when Ctrl-C is pressed, or the node is shutdown by the master.
    #
    rospy.spin()
