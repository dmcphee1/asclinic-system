#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/Int32.h"
#include "std_msgs/UInt16.h"
#include "std_msgs/Float64.h"

// Include the asclinic message types
#include "asclinic_pkg/VectorMessage.h" 
#include "asclinic_pkg/Vector_Array.h"

// Namespacing the package
using namespace asclinic_pkg;

static const int num_nodes = 20;
static const int node_index_i = 18;
static const int node_index_f = 19;

double vertices[num_nodes][2];

ros::Subscriber start_subscriber;
ros::Publisher path_publisher;

double adjacencyMatrix[num_nodes][num_nodes] = {
{0,0,1.0252,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.3447,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0.30583,0,0,0,0,0,0,0},
{1.0252,0,0,0,0,0,0.28198,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0.43014,0,0,0.87154,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0.43014,0,0,0,0,0,0.46487,0,0,0,0,0,0,0.32099,0,0,0},
{0,0,0,0,0,0,1.0428,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0.28198,0,0,1.0428,0,0,0,0,0,0,0,0,0,0.22443,0,0,0,0},
{0,0,0,0.87154,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0.72361,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0.46487,0,0,0,0.72361,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0.37396,0.38385,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0.37396,0,0,0,0.3295,0,0,0,0,0},
{0,0.30583,0,0,0,0,0,0,0,0,0.38385,0,0,0,0,0,0,0.3447,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.31531,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0.3295,0,0.31531,0,0,0,0,0,0},
{0,0,0,0,0,0,0.22443,0,0,0,0,0,0,0,0,0,0.61523,0,0,0},
{0,0,0,0,0.32099,0,0,0,0,0,0,0,0,0,0,0.61523,0,0,0,0},
{0.3447,0,0,0,0,0,0,0,0,0,0,0,0.3447,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0} };





double nodeLocations[num_nodes][2] = {
{ 2.25,3.0918 },
{ 3.15,3.265 },
{ 1.75,2.75 },
{ 1.4848,0.9225 },
{ 1.1769,1.2229 },
{ 0.65,2.915 },
{ 1.1832,2.3735 },
{ 2,0.55 },
{ 0.5,0.55 },
{ 0.9,0.84949 },
{ 3.1711,2.675 },
{ 3.1381,2.3025 },
{ 2.9823,3.0092 },
{ 3.1424,1.6725 },
{ 3.1564,1.9875 },
{ 1.1908,2.1525 },
{ 1.2074,1.5375 },
{ 2.5856,3.0153 },
{ 2,0.3 },
{ 3.4,1.5 } };

#include<iostream>
#include<stdio.h>
using namespace std;


void dijkstra(double G[num_nodes][num_nodes], int startnode, int max) {
    int n = num_nodes;
    double dist[max], pred[max], dist_min;
    int visited[max], count, nextnode;

    // Set all zero entries in adjacency matrix to 0 
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (G[i][j] == 0) G[i][j] = INFINITY;
        }
    }

    for (int i = 0; i < n; i++) {
        dist[i] = G[startnode][i];
        pred[i] = startnode;
        visited[i] = 0;
    }

    dist[startnode] = 0;
    visited[startnode] = 1;
    count = 1;

    while (count < n - 1) {
        dist_min = INFINITY;
        for (int i = 0; i < n; i++) {
            if (dist[i] < dist_min && !visited[i]) {
                dist_min = dist[i];
                nextnode = i;
            }
        }
        visited[nextnode] = 1;
        for (int i = 0; i < n; i++) {
            if (!visited[i] && dist_min + G[nextnode][i] < dist[i]) {
                dist[i] = dist_min + G[nextnode][i];
                pred[i] = nextnode;
            }
        }
        count++;
    }

    int endNode = node_index_f;
    int shortestPath[num_nodes];
    std::fill_n(shortestPath, num_nodes, -1);
    shortestPath[0] = endNode;
    int j = endNode, k = 1;
    do {
        j = pred[j];
        //ROS_INFO_STREAM("<-" << j);
        shortestPath[k++] = j;
    } while (j != startnode);

    // Print path vertex IDs 
    // This prints in the opposite direction
    int path_length = 0;
    while (shortestPath[path_length] >= 0) {
        ROS_INFO_STREAM(shortestPath[path_length++]);
    }

    // Convert to cartesian coordinates
    // Publish to trajectory generation
    Vector_Array path_msg = Vector_Array();
    for (int i = path_length - 1; i >= 0; i--) {
        VectorMessage waypoint = VectorMessage();
        waypoint.x = vertices[shortestPath[i]][0];
        waypoint.y = vertices[shortestPath[i]][1];
        path_msg.array.push_back(waypoint);
    }
    path_msg.size = path_length;
    path_publisher.publish(path_msg);

}


void find_path(const VectorMessage& vector_msg) {

    double G[num_nodes][num_nodes];
    std::copy(&adjacencyMatrix[0][0], &adjacencyMatrix[0][0] + num_nodes * num_nodes, &G[0][0]);

    //double vertices[num_nodes][2];
    std::copy(&nodeLocations[0][0], &nodeLocations[0][0] + num_nodes * 2, &vertices[0][0]);

    vertices[node_index_i][0] = vector_msg.x;
    vertices[node_index_i][1] = vector_msg.y;
    vertices[node_index_f][0] = vector_msg.z;
    vertices[node_index_f][1] = vector_msg.w;

    // Find closest node to initial_pos
    double min_dist = INFINITY;
    double dist;
    int closest_node = -1;
    for (int i = 0; i < num_nodes; i++) {
        dist = sqrt(pow(vector_msg.x - vertices[i][0], 2) + pow(vector_msg.y - vertices[i][1], 2));
        if (dist > 0 && dist < min_dist) {
            min_dist = dist;
            closest_node = i;
        }
    }

    ROS_INFO_STREAM("[PATH FINDING] Closest node to initial position = " << closest_node);

    // Add to adjacency matrix
    G[node_index_i][closest_node] = min_dist;
    G[closest_node][node_index_i] = min_dist;

    // Find closest node to final_pos
    min_dist = INFINITY;
    dist = -1;
    closest_node = -1;
    for (int i = 0; i < num_nodes; i++) {
        dist = sqrt(pow(vector_msg.z - vertices[i][0], 2) + pow(vector_msg.w - vertices[i][1], 2));
        if (dist > 0 && dist < min_dist) {
            min_dist = dist;
            closest_node = i;
        }
    }

    ROS_INFO_STREAM("[PATH FINDING] Closest node to final position = " << closest_node);

    // Add to adjacency matrix
    G[node_index_f][closest_node] = min_dist;
    G[closest_node][node_index_f] = min_dist;

    // Perform Dijkstra's algorithm
    dijkstra(G, node_index_i, num_nodes);
}




int main(int argc, char* argv[])
{
    ros::init(argc, argv, "path_finding");
    ros::NodeHandle nodeHandle("/path_finding");
    ros::Rate loop_rate(1);

    ROS_INFO_STREAM("[PATH FINDING] Initialised.");

    start_subscriber = nodeHandle.subscribe("/start_signal", 1, find_path);
    path_publisher = nodeHandle.advertise<Vector_Array>("/dijkstras/path", 10, false);

    while (ros::ok())
    {
        // Continually process callbacks
        ros::spin();
    }

    return 0;
}


/*
double x_initial = 0;
double y_initial = 0;
double x_final = 2;
double y_final = 2;
nodeLocations[node_index_i][0] = x_initial;
nodeLocations[node_index_i][1] = y_initial;
nodeLocations[node_index_f][0] = x_final;
nodeLocations[node_index_f][1] = y_final;
*/




