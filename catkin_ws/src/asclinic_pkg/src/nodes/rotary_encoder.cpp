#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/Int32.h"
#include "std_msgs/UInt16.h"

#include <gpiod.h>
#include <cstdio>

int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "rotary_encoder");
	ros::NodeHandle nodeHandle("~");

	// Initialise loop rate
	ros::Rate loop_rate(3200);

	// Specify the chip name of the GPIO interface
	const char * gpio_chip_name = "/dev/gpiochip0";

	// Get the GPIO line number to monitor
	unsigned int line_number_A = 133;
	unsigned int line_number_B = 134;

	unsigned int num_gpio_lines = 2;
	unsigned int gpio_lines[num_gpio_lines] = { line_number_A, line_number_B };


	// > Display the line number being monitored
	ROS_INFO_STREAM("[ROTARY ENCODER R] Will monitor lines "  << line_number_A << " and " << line_number_B);

	// Initialise and open GPIO chip
	struct gpiod_chip *chip;
	chip = gpiod_chip_open(gpio_chip_name);

	// Initialise and open GPIO lines
	struct gpiod_line* channelA;
	struct gpiod_line* channelB;
	channelA = gpiod_chip_get_line(chip, line_number_A);
	channelB = gpiod_chip_get_line(chip, line_number_B);

	int count = 0;
	int A_prev = -2;
	int B_prev = -2;
	int A_curr;
	int B_curr;

	// Enter a loop that continues while ROS is still running
	while (ros::ok())
	{
		A_curr = gpiod_ctxless_get_value(gpio_chip_name, line_number_A, false, "A");
		B_curr = gpiod_ctxless_get_value(gpio_chip_name, line_number_A, false, "B");

		// Respond only if the value is 0 or 1
		if ((A_curr == 0 || A_curr == 1) && (B_curr == 0 || B_curr == 1))
		{
			ROS_INFO_STREAM("[ROTARY ENCODER R] " << A_curr << ", " << B_curr);
			if (A_curr == A_prev && B_curr != B_prev)
			{
				// A changed
				if ((B_curr == 0 && A_curr == 1) || (A_curr == 0 && B_curr == 1)) {
					count++;
				}
				else {
					count--;
				}
			}
			else if (B_curr == B_prev && A_curr != A_prev)
			{
				// B changed
				if ((B_curr == 1 && A_curr == 1) || (A_curr == 0 && B_curr == 0)) {
					count++;
				}
				else {
					count--;
				}
			}
			else if (A_curr == A_prev && B_curr == B_prev)
			{
        // No change
      }
      else
      {
				ROS_INFO_STREAM("[ROTARY ENCODER R] Uh oh.");
			}


    ROS_INFO_STREAM("[ROTARY ENCODER R] " << count);

		A_prev = A_curr;
		B_prev = B_curr;

		loop_rate.sleep();
    }

  }
	// Close the GPIO chip
	//gpiod_chip_close(chip);

	return 0;
}
