// Copyright (C) 2021, The University of Melbourne, Department of Electrical and Electronic Engineering (EEE)
//
// This file is part of ASClinic-System.
//
// See the root of the repository for license details.
//
// ----------------------------------------------------------------------------
//     _    ____   ____ _ _       _          ____            _
//    / \  / ___| / ___| (_)____ (_) ___    / ___| _   _ ___| |_ ___ ________
//   / _ \ \___ \| |   | | |  _ \| |/ __|___\___ \| | | / __| __/ _ \  _   _ \
//  / ___ \ ___) | |___| | | | | | | (_|_____|__) | |_| \__ \ ||  __/ | | | | |
// /_/   \_\____/ \____|_|_|_| |_|_|\___|   |____/ \__, |___/\__\___|_| |_| |_|
//                                                 |___/
//
//
// ----------------------------------------------------------------------------


#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt16.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Int32.h"
#include <cmath>

float prevError = 0;
float currentError = 0;
float currentAngV =0;
float currentAngVreference = 0;
int speed = 0;
float errorSum = 0;


//Define constants
const float Ki = 0.26;
const float Kp = 5.7;
const float Kd = 1;

//const float Ki = 40.943;
//const float Kp = 4.8621;
const float T = 0.02;

//Function declarations
float PWMtoW(int PWM);
int applyPI(float currentError, float prevError);
int WtoPWM(float w);

// Respond to subscriber receiving a message
void PWM_reference_callback(const std_msgs::Int32& msg)
{
	//ROS_INFO_STREAM("[Reference speed] Message receieved with data ^^ = " << msg.data);
 currentAngVreference = msg.data;
}

void angV_callback(const std_msgs::Float64& msg)
{
	//ROS_INFO_STREAM("[AngVCallback] Message receieved with data ^^ = " << msg.data);
 currentAngV = msg.data;
 currentAngV = -1*currentAngV;

}


int main(int argc, char* argv[])
{



	// Initialise the node
	ros::init(argc, argv, "dynamics_controller_node");
	ros::NodeHandle nodeHandle("/dynamics_controller_node");
	// Initialise a publisher


 //USE THIS PUBLISHER TO SET UP INTERNAL CONTROL
  //ros::Publisher rightPWMpublisher = nodeHandle.advertise<std_msgs::Int32>("/motor_node/rightPWM", 10, false);

	// Initialise a subscriber
	//Subscribe to the PWM reference
	ros::Subscriber PWM_reference_subscriber = nodeHandle.subscribe("test_duty_cycle", 1, PWM_reference_callback);
  //Subcribe to the wheel velocity topic
  ros::Subscriber Right_angV_subscriber = nodeHandle.subscribe("angVLeft", 1, angV_callback);


	// Initialise a variable with loop rate for
	ros::Rate loop_rate(50);

	while (ros::ok())
	{
   //Change PWM to w
   currentAngVreference = PWMtoW(currentAngVreference);

		//Calculate current error in w units
   currentError = currentAngVreference-currentAngV;

   //Apply difference Eqn
   //speed = applyPI(currentError, prevError);

   //Accumulate error
   if (Ki*errorSum>100 && currentError>0)
   {

   }
   else if (Ki*errorSum <-100 && currentError<0)
   {

   }
   else
   {
   errorSum += currentError;
   }



   //Apply PID
   speed = Kp*currentError + Ki*errorSum + Kd*(currentError-prevError)/T;

   //Convert to PWM to send to motor
   speed = WtoPWM(speed);


   //ROS_INFO_STREAM("[Controller] Speed   = " << speed <<"Ang V = "<< currentAngV);

   //Prepare for next time step
   prevError = currentError;


    // Publish a message
		//std_msgs::Int32 msg;
    //msg.data = speed;
	  //rightPWMpublisher.publish(msg);


		// Spin once so that this node can service any
		// callbacks that this node has queued.
		ros::spinOnce();

		// Sleep for the specified loop rate
		loop_rate.sleep();
	}

	return 0;
}


float PWMtoW(int PWM)
{
  return (0.241*PWM - 0.225)/32;
}

int applyPI(float currentError, float prevError)
{
  return (T*Ki/2.0 + Kp)*currentError + (T*Ki/2.0 - Kp)*prevError +1;
}

int WtoPWM(float w)
{

  float value = round(32*(4.1494*w + 0.9336));
  return value;
}
