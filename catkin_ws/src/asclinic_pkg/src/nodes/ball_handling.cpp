// Copyright (C) 2021, The University of Melbourne, Department of Electrical and Electronic Engineering (EEE)
//
// This file is part of ASClinic-System.
//
// See the root of the repository for license details.
//
// ----------------------------------------------------------------------------
//     _    ____   ____ _ _       _          ____            _
//    / \  / ___| / ___| (_)____ (_) ___    / ___| _   _ ___| |_ ___ ________
//   / _ \ \___ \| |   | | |  _ \| |/ __|___\___ \| | | / __| __/ _ \  _   _ \
//  / ___ \ ___) | |___| | | | | | | (_|_____|__) | |_| \__ \ ||  __/ | | | | |
// /_/   \_\____/ \____|_|_|_| |_|_|\___|   |____/ \__, |___/\__\___|_| |_| |_|
//                                                 |___/
//
// DESCRIPTION:
// Template node for mointoring edge events on a GPIO pin
//
// ----------------------------------------------------------------------------






#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/Int32.h"
#include "std_msgs/UInt16.h"
#include "std_msgs/Float64.h"
#include "asclinic_pkg/VectorMessage.h"
#include "asclinic_pkg/Vector_Array.h"
#include <gpiod.h>
#include <unistd.h>

// Namespacing the package
using namespace asclinic_pkg;

int gpioOut=0;
bool flag = false;
float x_current;
float y_current;
float tol=0.1; 
float x_disp_pos = 1.5;
float y_disp_pos = 0.3;
float x_recept_pos1 = 1.2;
float y_recept_pos1 = 3.26;
float x_recept_pos2 = 3.3;
float y_recept_pos2 = 2.38;
int run_number = 1;
VectorMessage newMessage = VectorMessage();


// x = 1.2, y = 0.3 This is the dispenser closest to entrance TO GET TO HERE, SEND [0.3 0.3 1.2 0.3] in the starting command
// x  = 2.24 y = 0.56 Dropper dispensor
// x = 3.2 , y=0.4 The floor height recepticle

// Respond to subscriber receiving a message
void x_Callback(const std_msgs::Float64& msg)
{ 
 ROS_INFO_STREAM("[X Callback.] Message receieved with data = " << msg.data);
 x_current = msg.data;
}

void y_Callback(const std_msgs::Float64& msg)
{
	//WORKING
  ROS_INFO_STREAM("[Y Callback.] Message receieved with data = " << msg.data);
  y_current = msg.data;
}


int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "ball_handling_node");
	ros::NodeHandle nodeHandle("/ball_handling_node");

	// Initialise a publisher
  ros::Publisher nextNode_publisher = nodeHandle.advertise<asclinic_pkg::VectorMessage>("/start_signal", 10, false);
  

	// Initialise a subscriber
	// > Note that the subscriber is included only for the purpose
	//   of demonstrating this template node running stand-alone
	//ros::Subscriber gpio_event_subscriber = nodeHandle.subscribe("gpio_event", 1, templateSubscriberCallback);
  ros::Subscriber odom_x_subscriber = nodeHandle.subscribe("/odom/x", 1, x_Callback);
  ros::Subscriber odom_y_subscriber = nodeHandle.subscribe("/odom/y", 1, y_Callback);


  ros::Rate loop_rate(0.08);

	// Specify the chip name of the GPIO interface
	// > Note: for the 40-pin header of the Jetson SBCs, this
	//   is "/dev/gpiochip0"
	const char * gpio_chip_name = "/dev/gpiochip0";

	// Get the GPIO line number to monitor
	// Notes:
	// > If you look at the "template_gpio.launch" file located in
	//   the "launch" folder, you see the following lines of code:
	//       <param
	//           name   = "line_number"
	//           value  = 148
	//       />
	// > These lines of code add a parameter named "line_number"
	//   to the this node.
	// > Thus, to access this "line_number" parameter, we first
	//   get a handle to this node within the namespace that it
	//   was launched.
	//std::string namespace = ros::this_node::getNamespace();
	int line_number = 0;
	if ( !nodeHandle.getParam("line_number", line_number) )
	{
		ROS_INFO("[TEMPLATE GPIO EVENT TRIG.] FAILED to get \"line_number\" parameter. Using default value instead.");
		// Set the line number to a default value
		line_number = 148;
	}
	// > Display the line number being monitored
	ROS_INFO_STREAM("[TEMPLATE GPIO EVENT TRIG.] Will monitor \"line_number\" = " << line_number);

	// Initialise a GPIO chip, line, and event object
	struct gpiod_chip *chip;
	struct gpiod_line *line;
	struct gpiod_line_event event;



	// Intialise a variable for the flags returned
	// by GPIO calls
	int returned_wait_flag;
	int returned_read_flag;


	// Get and print the value of the GPIO line
	// > Note: the third argument to "gpiod_ctxless_get_value"
	//   is an "active_low" boolean input argument.
	//   If true, this indicate to the function that active state
	//   of this line is low.
	int value=0;
	value = gpiod_ctxless_get_value(gpio_chip_name, line_number, false, "foobar");
	ROS_INFO_STREAM("[TEMPLATE GPIO EVENT TRIG.] On startup of node, chip " << gpio_chip_name << " line " << line_number << " returned value = " << value);

	// Open the GPIO chip
	chip = gpiod_chip_open(gpio_chip_name);
	// Retrieve the GPIO line
	line = gpiod_chip_get_line(chip,line_number);
	// Display the status
	ROS_INFO_STREAM("[TEMPLATE GPIO EVENT TRIG.] Chip " << gpio_chip_name << " opened and line " << line_number << " retrieved");

  int ret = gpiod_line_request_output(line, "gpio", 0);
  ret = gpiod_line_set_value(line, gpioOut);


	// Enter a loop that continues while ROS is still running
	while (ros::ok())
	{
   ros::spinOnce();
   if (run_number==1)
   {
    if (flag==false)
    {
      if ((x_current<x_disp_pos+tol) &&( x_current>x_disp_pos-tol))
      {
        if ((y_current<y_disp_pos+tol) &&(y_current>y_disp_pos-tol))
        {
          flag = true;
          gpioOut = 1;
          ret = gpiod_line_set_value(line, gpioOut);
          
	        newMessage.x = 1.2;
	        newMessage.y = 0.3;
          newMessage.z = 1.2;
	        newMessage.w = 3.26;
          nextNode_publisher.publish(newMessage);
        }
      }
    
    }
    
    else if (flag == true)
    {
      if ((x_current<x_recept_pos1+tol) &&( x_current>x_recept_pos1-tol))
      {
        if ((y_current<y_recept_pos1+tol) &&(y_current>y_recept_pos1-tol))
        {
          flag = false;
          gpioOut = 0;
          ret = gpiod_line_set_value(line, gpioOut);
          run_number=2;
          
	        newMessage.x = 1.2;
	        newMessage.y = 3.26;
          newMessage.z = 1.2;
	        newMessage.w = 0.3;
          nextNode_publisher.publish(newMessage);
        }
      }
    }
   }
   
   
   
   if (run_number==2)
   {
    if (flag==false)
    {
      if ((x_current<x_disp_pos+tol) &&( x_current>x_disp_pos-tol))
      {
        if ((y_current<y_disp_pos+tol) &&(y_current>y_disp_pos-tol))
        {
          flag = true;
          gpioOut = 1;
          ret = gpiod_line_set_value(line, gpioOut);
    
	        newMessage.x = 1.2;
	        newMessage.y = 0.3;
          newMessage.z = 3.3;
	        newMessage.w = 2.38;
          nextNode_publisher.publish(newMessage);
        }
      }
    
    }
    
    else if (flag == true)
    {
      if ((x_current<x_recept_pos2+tol) &&( x_current>x_recept_pos2-tol))
      {
        if ((y_current<y_recept_pos2+tol) &&(y_current>y_recept_pos2-tol))
        {
          flag = false;
          gpioOut = 0;
          ret = gpiod_line_set_value(line, gpioOut);
          run_number=2;
        }
      }
    }
   }
    
    /*ret = gpiod_line_set_value(line, 1);
    ros::Duration(1.5).sleep();
     
    ret = gpiod_line_set_value(line, 0);
    ros::Duration(1.5).sleep();*/
      
    
    //ROS_INFO_STREAM("[BALL HANDLER] " << gpioOut);
    loop_rate.sleep();
	} // END OF: "while (ros::ok())"

	// Close the GPIO chip
	gpiod_chip_close(chip);

	return 0;
}
