// Copyright (C) 2021, The University of Melbourne, Department of Electrical and Electronic Engineering (EEE)
//
// This file is part of ASClinic-System.
//
// See the root of the repository for license details.
//
// ----------------------------------------------------------------------------
//     _    ____   ____ _ _       _          ____            _
//    / \  / ___| / ___| (_)____ (_) ___    / ___| _   _ ___| |_ ___ ________
//   / _ \ \___ \| |   | | |  _ \| |/ __|___\___ \| | | / __| __/ _ \  _   _ \
//  / ___ \ ___) | |___| | | | | | | (_|_____|__) | |_| \__ \ ||  __/ | | | | |
// /_/   \_\____/ \____|_|_|_| |_|_|\___|   |____/ \__, |___/\__\___|_| |_| |_|
//                                                 |___/
//
//
// ----------------------------------------------------------------------------



#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt16.h"
#include "std_msgs/Float64.h"
#include <math.h>

// Include the asclinic message types
#include "asclinic_pkg/VectorMessage.h"
#include "asclinic_pkg/Vector_Array.h"

// Namespacing the package
using namespace asclinic_pkg;

ros::Publisher test_publisher;
ros::Publisher trajectory_publisher;

//float plannedPath[100][2];
bool isDriving = false;
int updateRate = 5;

float pose_current[3] = {0.3, 0.3, 0};

void poseCallback(const VectorMessage& data)
{
  pose_current[0] = data.x;
  pose_current[1] = data.y;
  pose_current[2] = data.z;
}

 
VectorMessage generateAngularTrajectory(VectorMessage lastTrajectory, float angInc, float desiredHeading, int increment, int numOfIncrements){
	VectorMessage newTrajectory = VectorMessage();
	newTrajectory.x = lastTrajectory.x;
	newTrajectory.y = lastTrajectory.y;

	//for purely rotational
	newTrajectory.w = 1;

	if(increment == numOfIncrements){
		newTrajectory.z = desiredHeading;
	}
	else{
		newTrajectory.z = lastTrajectory.z + angInc;
	}

	return newTrajectory;
}

VectorMessage generateLinearTrajectory(VectorMessage lastTrajectory, float xIncrement, float yIncrement, float desiredPos[], int increment, int numOfIncrements){
	VectorMessage newTrajectory = VectorMessage();
	newTrajectory.z = lastTrajectory.z;

	//for linear
	newTrajectory.w = 0;

	if(increment == numOfIncrements){
		newTrajectory.x = desiredPos[0];
		newTrajectory.y = desiredPos[1];
	}
	else{
		newTrajectory.x = lastTrajectory.x + xIncrement;
		newTrajectory.y = lastTrajectory.y + yIncrement;
	}

	return newTrajectory;
}


void generateTrajectory(const Vector_Array& path_coords) {

  /*
  ROS_INFO_STREAM("path coordinate = " << path_coords.array[0].x);
  ROS_INFO_STREAM("path coordinate = " << path_coords.array[0].x);
  ROS_INFO_STREAM("path coordinate = " << path_coords.array[0].x);
  ROS_INFO_STREAM("path coordinate = " << path_coords.array[0].x);
  ROS_INFO_STREAM("path coordinate = " << path_coords.array[0].x);
  ROS_INFO_STREAM("path coordinate = " << path_coords.array[0].x);
  ROS_INFO_STREAM("path coordinate = " << path_coords.array[0].x);
  VectorMessage test = VectorMessage();
  test.x = 1.0;
  test_publisher.publish(test);
  */

  float plannedPath[path_coords.size][2];
  for (int i = 0; i < path_coords.size; i++) {
    plannedPath[i][0] = path_coords.array[i].x;
    plannedPath[i][1] = path_coords.array[i].y;
  }
  int nodes = path_coords.size;
  
  float linSpeed = 0.1;
	float angSpeed = 0.1;
	float tDelta = 1.0/(float)updateRate;

	int i = 0; // Keeps track of what segment you are on
	int j = 0; // Keeps track of what angular increment you are on
	int k = 0; // Keeps track of what linear increment you are on


	VectorMessage trajectory = VectorMessage();
	trajectory.x = pose_current[0];
	trajectory.y = pose_current[1];
	trajectory.z = pose_current[2];
 
 for (int i = 0; i < path_coords.size; i++) {
   VectorMessage test = VectorMessage();
   test.x = plannedPath[i][0];
   test.y = plannedPath[i][1];
   test_publisher.publish(test);
   }

  // This has been made negative
	float desiredHeading = atan2(plannedPath[i+1][1] - plannedPath[i][1], plannedPath[i+1][0] - plannedPath[i][0]);
	float angDiff = abs(trajectory.z - desiredHeading);
	float numOfIncrementsAng = (int)(angDiff/(angSpeed*tDelta));
	float angleIncrement = (desiredHeading - trajectory.z)/numOfIncrementsAng;

	float linIncrement = tDelta*linSpeed;
	float xDiff = plannedPath[i+1][0] - plannedPath[i][0];
	float yDiff = plannedPath[i+1][1] - plannedPath[i][1];
	float dist = sqrt(xDiff*xDiff + yDiff*yDiff);
	float numOfIncrementsLin = dist/linIncrement;
	float xIncrement = xDiff/numOfIncrementsLin;
	float yIncrement = yDiff/numOfIncrementsLin;

  ros::Rate loop_rate(updateRate);

	while (ros::ok())
	{

		// VectorMessage test = VectorMessage();
		// test.x = i;
		// test.y = j;
		// test.z = k;
		//
		// trajectory_publisher.publish(test);


		if (j <= numOfIncrementsAng){
			trajectory = generateAngularTrajectory(trajectory, angleIncrement, desiredHeading, j, numOfIncrementsAng);
			j++;
		}
		else if (k <= numOfIncrementsLin){
			trajectory = generateLinearTrajectory(trajectory, xIncrement, yIncrement, plannedPath[i+1], k, numOfIncrementsLin);
			k++;
		} else {
			i++;

			//Reached the end;
			if(i >= nodes-1){
				VectorMessage endTrajectory = VectorMessage();
        endTrajectory.w = -1;
        trajectory_publisher.publish(endTrajectory);
        return;
			}

			//Reset segment increment;
			j = 0;
			k = 0;

			desiredHeading = atan2(plannedPath[i+1][1] - plannedPath[i][1], plannedPath[i+1][0] - plannedPath[i][0]);
			angDiff = abs(trajectory.z - desiredHeading);
			numOfIncrementsAng = (int)(angDiff/(angSpeed*tDelta));
			angleIncrement = (desiredHeading - trajectory.z)/numOfIncrementsAng;

			linIncrement = tDelta*linSpeed;
			xDiff = plannedPath[i+1][0] - plannedPath[i][0];
			yDiff = plannedPath[i+1][1] - plannedPath[i][1];
			dist = sqrt(xDiff*xDiff + yDiff*yDiff);
			numOfIncrementsLin = dist/linIncrement;
			xIncrement = xDiff/numOfIncrementsLin;
			yIncrement = yDiff/numOfIncrementsLin;

		}

		trajectory_publisher.publish(trajectory);


		// Spin once so that this node can service any
		// callbacks that this node has queued.
		ros::spinOnce();

		// Sleep for the specified loop rate
		loop_rate.sleep();
	}
}



int main(int argc, char* argv[])
{


	// Initialise the node
	ros::init(argc, argv, "trajectory_node");
	ros::NodeHandle nodeHandle("/trajectory_node");
 
 	// Initialise a variable for loop rate
  ros::Rate loop_rate(updateRate);
 
	// Initialise a publisher
	trajectory_publisher = nodeHandle.advertise<VectorMessage>("trajectory", 10, false);
  test_publisher = nodeHandle.advertise<VectorMessage>("test", 10, false);
	// Initialise a subscriber
	ros::Subscriber path_subscriber = nodeHandle.subscribe("/dijkstras/path", 1, generateTrajectory);
  ros::Subscriber odom_subscriber = nodeHandle.subscribe("/odom/odom_pose_vector", 1, poseCallback);
	
 	while (ros::ok()) {
  
    ros::spin();
  }
 
	return 0;
}
