// Copyright (C) 2021, The University of Melbourne, Department of Electrical and Electronic Engineering (EEE)
//
// This file is part of ASClinic-System.
//
// See the root of the repository for license details.
//
// ----------------------------------------------------------------------------
//     _    ____   ____ _ _       _          ____            _
//    / \  / ___| / ___| (_)____ (_) ___    / ___| _   _ ___| |_ ___ ________
//   / _ \ \___ \| |   | | |  _ \| |/ __|___\___ \| | | / __| __/ _ \  _   _ \
//  / ___ \ ___) | |___| | | | | | | (_|_____|__) | |_| \__ \ ||  __/ | | | | |
// /_/   \_\____/ \____|_|_|_| |_|_|\___|   |____/ \__, |___/\__\___|_| |_| |_|
//                                                 |___/
//
//
// ----------------------------------------------------------------------------


#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/Int32.h"
#include "std_msgs/Float64.h"
#include<cmath>
// Include the asclinic message types
#include "asclinic_pkg/VectorMessage.h"

int WtoPWM(float w,float v);

float para_error = 0;
float para_error_prev = 0;
float total_para_error = 0;
float perp_error = 0;
float perp_error_prev = 0;
float total_perp_error = 0;
float leftPWM = 0;
float rightPWM = 0;
float eps = 0.01;

//const float Kv = 1.2;
//const float Kw = 1.5;
const float Kv = 2;
const float Kw = 2;
const float b = 0.25; //B is the axle length
const float r = 0.1; //R is the wheel radius

// Respond to subscriber receiving a message
void errorSubscriberCallback(const asclinic_pkg::VectorMessage& msg)
{
 //ROS_INFO_STREAM("[Kinematic Controller] Para Error = " << para_error << "  Perp Error = " << perp_error << "  LeftPWM = "<< leftPWM << "  RightPWM = " <<rightPWM);
 para_error_prev = para_error;
 para_error = msg.x;
 total_para_error += para_error;

 perp_error_prev = perp_error;
 perp_error = msg.y;
 total_perp_error += perp_error;
}

int main(int argc, char* argv[])
{




	// Initialise the node
	ros::init(argc, argv, "kinematic_controller_node");
	ros::NodeHandle nodeHandle("/kinematic_controller_node");
	// Initialise a publisher
  //ros::Publisher motor_control_input_publisher = nodeHandle.advertise<std_msgs::Int32>("kinematic_control_input", 10, false);
  
  //UNCOMMENT THESE TWO
  ros::Publisher rightPWMpublisher = nodeHandle.advertise<std_msgs::Int32>("/motor_node/rightPWM", 10, false);
  ros::Publisher leftPWMpublisher = nodeHandle.advertise<std_msgs::Int32>("/motor_node/leftPWM", 10, false);
  
  ros::Publisher parallelPub = nodeHandle.advertise<std_msgs::Float64>("parallelError", 10, false);
  ros::Publisher perpPub = nodeHandle.advertise<std_msgs::Float64>("perpError", 10, false);

	// Initialise a subscriber
	//Subscribe to Error
	ros::Subscriber kinematic_error_subscriber = nodeHandle.subscribe("/error_function/pose_error", 1, errorSubscriberCallback);


	// Initialise a variable with loop rate for
	ros::Rate loop_rate(50);

	while (ros::ok())
	{
		//Calculate control input for v
		float v = Kv * para_error; //+(Kv/1)*total_para_error;
		float w = Kw * perp_error;// + (Kv/2)*(perp_error - perp_error_prev)/50;

		float angVl = v/r - b*w/r;
		float angVr = v/r + b*w/r;


    //Output w as PWM in the form of a percent. Use 5 or 10 per cent
    leftPWM = WtoPWM(angVl,v);
    rightPWM = WtoPWM(angVr,v);




    // Publish a message
		
   
    std_msgs::Int32 msgRight;
    msgRight.data = rightPWM;
		rightPWMpublisher.publish(msgRight);

    std_msgs::Int32 msgLeft;
    msgLeft.data = leftPWM;
		leftPWMpublisher.publish(msgLeft);

    std_msgs::Float64 msgPerp;
    msgPerp.data = perp_error;
		perpPub.publish(msgPerp);

    std_msgs::Float64 msgPara;
    msgPara.data = para_error;
		parallelPub.publish(msgPara);

		// Spin once so that this node can service any
		// callbacks that this node has queued.
		ros::spinOnce();

		// Sleep for the specified loop rate
		loop_rate.sleep();
	}

	return 0;
}

int WtoPWM(float w, float v)
{
  int value = (w/22.0)*3200;
  if (abs(value-(v/r))>5)
  {
    if (value<0)
    {
      value = value - 350;
    }
    else if (value > 0)
    {
      value = value + 350;
    }
  }
  return value;
  //return (w/22.0)*100;
}
