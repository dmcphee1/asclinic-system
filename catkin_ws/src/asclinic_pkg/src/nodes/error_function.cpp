#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/Int32.h"
#include "std_msgs/UInt16.h"
#include "std_msgs/Float64.h"

// Include the asclinic message types
#include "asclinic_pkg/VectorMessage.h"

// Namespacing the package
using namespace asclinic_pkg;

#include <gpiod.h>
#include <math.h>

// Declare publishers and subscribers
ros::Publisher pose_error_publisher;
ros::Publisher x_refrence_publisher;
ros::Publisher y_refrence_publisher;
ros::Publisher phi_refrence_publisher;
ros::Subscriber trajectory_subscriber;
ros::Subscriber odom_subscriber;

// Used for sending data to matlab via refrence publishers
std_msgs::Float64 msg_out;

// Initialise trajectory and pose
float trajectory[4] = { 0, 0, 0, 100 };
float pose[3] = { 0, 0, 0 };

void calculate_error()
{
  float parallel_error;
  float perp_error;
  if (trajectory[3] < 0.5 && trajectory[3] > -0.5) {
    // Calculate magnitude and angle of error
    float dx = trajectory[0] - pose[0];
    float dy = trajectory[1] - pose[1];
    float d_magnitude = sqrt((dx * dx) + (dy * dy));
    float d_angular = atan2(dy, dx);

    // Calculate parallel and perpendicular error
    float phi_desired = trajectory[2];
    perp_error = d_magnitude * sin(d_angular - phi_desired);
    parallel_error = d_magnitude * cos(d_angular - phi_desired);
  } else if (trajectory[3] >= 0.5 && trajectory[3]){
    perp_error = 0.2 * (trajectory[2] - pose[2]);
    parallel_error = 0;
  }else if (trajectory[3] < -0.5 && trajectory[3] > -1.5){
    perp_error = 0;
    parallel_error = 0;
  }else {
    return;
  }
  ROS_INFO_STREAM("Trajectory = [" << trajectory[0] << ", " << trajectory[1] << ", " << trajectory[2] << "]." );
  ROS_INFO_STREAM("Odometry = [" << pose[0] << ", " << pose[1] << ", " << pose[2] << "]." );
  // Publish error signals
  VectorMessage error_msg = VectorMessage();
  error_msg.x = parallel_error;
  error_msg.y = perp_error;
  pose_error_publisher.publish(error_msg);
}

void update_trajectory(const VectorMessage& vector_msg)
{
    // Update trajectory and calculate error
    trajectory[0] = vector_msg.x;
    trajectory[1] = vector_msg.y;
    trajectory[2] = vector_msg.z;
    trajectory[3] = vector_msg.w;

    msg_out.data = trajectory[0];
    x_refrence_publisher.publish(msg_out);
    msg_out.data = trajectory[1];
    y_refrence_publisher.publish(msg_out);
    msg_out.data = trajectory[2];
    phi_refrence_publisher.publish(msg_out);

	calculate_error();
}

void update_pose(const VectorMessage& vector_msg)
{
    // Update pose and calculate error
	pose[0] = vector_msg.x;
  pose[1] = vector_msg.y;
  pose[2] = vector_msg.z;
	calculate_error();
}

int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "error_function");
	ros::NodeHandle nodeHandle("/error_function");

	ROS_INFO_STREAM("[ERROR FUNCTION] Initialised.");


    // Initialise publishers
	pose_error_publisher = nodeHandle.advertise<VectorMessage>("pose_error", 10, false);
    x_refrence_publisher = nodeHandle.advertise<std_msgs::Float64>("x_refrence", 10, false);
    y_refrence_publisher = nodeHandle.advertise<std_msgs::Float64>("y_refrence", 10, false);
    phi_refrence_publisher = nodeHandle.advertise<std_msgs::Float64>("phi_refrence", 10, false);


 	// Initialise subscribers
    trajectory_subscriber = nodeHandle.subscribe("/trajectory_node/trajectory", 1, update_trajectory);               // UPDATE THIS
    odom_subscriber = nodeHandle.subscribe("/odom/odom_pose_vector", 1, update_pose);                                           // UPDATE THIS


    while(ros::ok())
    {
        // Continually process callbacks
        ros::spin();
    }

	return 0;
}
