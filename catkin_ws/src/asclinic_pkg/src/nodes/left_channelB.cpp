
#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/Int32.h"
#include "std_msgs/UInt16.h"

#include <gpiod.h>


int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "left_channelB");
	ros::NodeHandle nodeHandle("/LeftB");


  ros::Publisher publisher = nodeHandle.advertise<std_msgs::Int32>("value", 10, false);


	// Specify the chip name of the GPIO interface
	// > Note: for the 40-pin header of the Jetson SBCs, this
	//   is "/dev/gpiochip0"
	const char * gpio_chip_name = "/dev/gpiochip0";

	int line_number = 134;

	// > Display the line number being monitored
	ROS_INFO_STREAM("[LEFT CHANNEL B] Will monitor \"line_number\" = " << line_number);

	// Initialise a GPIO chip, line, and event object
	struct gpiod_chip *chip;
	struct gpiod_line *line;
	struct gpiod_line_event event;

	// Specify the timeout specifications
	// > The first entry is seconds
	// > The second entry is nano-seconds
	struct timespec ts = { 0, 100000000 };

	// Intialise a variable for the flags returned
	// by GPIO calls
	int returned_wait_flag;
	int returned_read_flag;


	// Open the GPIO chip
	chip = gpiod_chip_open(gpio_chip_name);
	// Retrieve the GPIO line
	line = gpiod_chip_get_line(chip,line_number);
	// Display the status
	ROS_INFO_STREAM("[LEFT CHANNEL B] Chip " << gpio_chip_name << " opened and line " << line_number << " retrieved");

	// Request the line events to be mointored
	// > Note: only one of these should be uncommented
	//gpiod_line_request_rising_edge_events(line, "foobar");
	//gpiod_line_request_falling_edge_events(line, "foobar");
	gpiod_line_request_both_edges_events(line, "foobar");

	// Display the line event values for rising and falling
	ROS_INFO_STREAM("[LEFT CHANNEL B] The constants defined for distinguishing line events are:, GPIOD_LINE_EVENT_RISING_EDGE = " << GPIOD_LINE_EVENT_RISING_EDGE << ", and GPIOD_LINE_EVENT_FALLING_EDGE = " << GPIOD_LINE_EVENT_FALLING_EDGE);

	// Enter a loop that continues while ROS is still running
	while (ros::ok())
	{

		// Monitor for the requested events on the GPIO line
		// > Note: the function "gpiod_line_event_wait" returns:
		//    0  if wait timed out
		//   -1  if an error occurred
		//    1  if an event occurred.
		returned_wait_flag = gpiod_line_event_wait(line,&ts);

		// Respond based on the the return flag
		switch (returned_wait_flag)
		{
			// Event occurred:
			case 1:
			{
				// Read the pending event on the GPIO line
				// > Note: the function "gpiod_line_event_read" returns:
				//    0  if the event was read correctly
				//   -1  if an error occurred
				returned_read_flag = gpiod_line_event_read(line,&event);


				switch (returned_read_flag)
				{
					// Event read correctly
					case 0:
					{



							// Display the event
							//ROS_INFO_STREAM("event type = " << event.event_type);


              // Publish a message
							std_msgs::Int32 msg;
              msg.data = event.event_type;
							publisher.publish(msg);
              //ROS_INFO_STREAM("event type = " << event.event_type << ", time delta (sec) = " << this_diff_sec);
							ros::spinOnce();

					}

					// Error occurred
					case -1:
					{
						// Display the status
						//ROS_INFO("[LEFT CHANNEL B] gpiod_line_event_wait returned the status that an error occurred");
						break;
					}

					default:
					{
						// Display the status
						//ROS_INFO_STREAM("[LEFT CHANNEL B] gpiod_line_event_read returned an unrecognised status, return_flag =  " << returned_read_flag );
						break;
					}
				} // END OF: "switch (returned_read_flag)"
				break;
			}

			// Time out occurred
			case 0:
			{
				// Spin once so that this node can service the publishing
				// of this message, and any other callbacks that this
				// node has queued
				ros::spinOnce();
				break;
			}

			// Error occurred
			case -1:
			{
				// Display the status
				//ROS_INFO("[LEFT CHANNEL B] gpiod_line_event_wait returned the status that an error occurred");
				break;
			}

			default:
			{
				// Display the status
				//ROS_INFO_STREAM("[LEFT CHANNEL B] gpiod_line_event_wait returned an unrecognised status, return_flag =  " << returned_wait_flag );
				break;
			}
		} // END OF: "switch (returned_wait_flag)"
	} // END OF: "while (ros::ok())"

	// Close the GPIO chip
	gpiod_chip_close(chip);

	return 0;
}
