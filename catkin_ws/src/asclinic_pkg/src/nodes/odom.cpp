#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/Int32.h"
#include "std_msgs/UInt16.h"
#include "std_msgs/Float64.h"
#include "asclinic_pkg/VectorMessage.h"

using namespace asclinic_pkg;

#include <gpiod.h>
#include <math.h>


  int right_count = 0;
  int left_count = 0;
  int prev_right_count = 0;
  int prev_left_count = 0;

  float x = 0.3;
  float y = 0.3;
  float phi = 0;

  //ros::Time current_time = 0;
  //ros::Time prev_time = 0;

  std_msgs::Float64 msg_out;
  std_msgs::Float64 angVel_out;



void left_count_update(const std_msgs::Int32& msg_in)
{
  left_count = msg_in.data;
  //ROS_INFO_STREAM("[LEFT_COUNT] " << left_count);
}

void right_count_update(const std_msgs::Int32& msg_in)
{
  right_count = msg_in.data;
  //ROS_INFO_STREAM("[RIGHT_COUNT] " << right_count);
}

void reset_odom(const std_msgs::Int32& msg_in) {
  x = 0;
  y = 0;
  phi = 0;
}

int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "odom");
	ros::NodeHandle nodeHandle("/odom");

	ROS_INFO_STREAM("[ODOMETRY] Initialised.");


  ros::Rate loop_rate(50);
  float dt = 1.0 / 50;

  float r = 0.05;
  float b2 = 0.25;
  float theta_l = 0;
  float theta_r = 0;


	// Initialise a "VectorMessage"
	// > Note that all value are set to zero by default
	//VectorMessage odom_pose_msg = VectorMessage();
  //VectorMessage ang_vel_msg = VectorMessage();
	// Set the values

	// Publish the message

  ros::Publisher odom_pose_publisher = nodeHandle.advertise<VectorMessage>("odom_pose_vector", 10, false);
  //ros::Publisher ang_vel_publisher = nodeHandle.advertise<VectorMessage>("ang_vel_vector", 10, false);


	ros::Publisher x_publisher = nodeHandle.advertise<std_msgs::Float64>("x", 10, false);
 	ros::Publisher y_publisher = nodeHandle.advertise<std_msgs::Float64>("y", 10, false);
 	ros::Publisher phi_publisher = nodeHandle.advertise<std_msgs::Float64>("phi", 10, false);

  ros::Publisher angVLeft_publisher = nodeHandle.advertise<std_msgs::Float64>("/dynamics_controller_node/angVLeft",10,false);
  ros::Publisher angVRight_publisher = nodeHandle.advertise<std_msgs::Float64>("angVRight",10,false);


 	// Initialise a subscriber
  ros::Subscriber subscr_count_left = nodeHandle.subscribe("/encoder_hub/left_count", 1, left_count_update);
  ros::Subscriber subscr_count_right = nodeHandle.subscribe("/encoder_hub/right_count", 1, right_count_update);
  //ros::Subscriber reset_odom = nodeHandle.subscribe("/reset_odom", 1, reset_odom);

  while(ros::ok())
  {
    float d_theta_l = 2 * M_PI * (left_count - prev_left_count) / 3200;
    float d_theta_r = 2 * M_PI * (right_count - prev_right_count) / 3200;
    float d_s = 0.5 * r * (d_theta_r - d_theta_l);
    float d_phi = r * (d_theta_r + d_theta_l) / b2;
    float d_x = d_s * cos(phi + 0.5 * d_phi);
    float d_y = d_s * sin(phi + 0.5 * d_phi);

   	//ROS_INFO_STREAM("[ENCODER HUB] " << d_theta_l << ", " << d_theta_r << ", " << d_s << ", " << d_phi << ", " << d_x << ", " << d_y);
    //ROS_INFO_STREAM("[ENCODER HUB] " << d_theta_r << ", " << d_theta_l << ", " << d_theta_r - d_theta_l << "," << d_s);
   	//ROS_INFO_STREAM("[ENCODER HUB] " << x << ", " << y << ", " << phi);

    float left_angV = d_theta_l/dt;
    float right_angV = d_theta_r/dt;

    // Update pose
    x += d_x;
    y += d_y;
    phi -= d_phi;
    
    theta_l += d_theta_l;
    theta_r += d_theta_r;

    // Publish updated pose

    VectorMessage odom_pose_msg = VectorMessage();
    odom_pose_msg.x = x;
    odom_pose_msg.y = y;
    odom_pose_msg.z = phi;
    odom_pose_publisher.publish(odom_pose_msg);

    /*
    ang_vel_msg.vector.push_back(left_angV);
    ang_vel_msg.vector.push_back(right_angV);
    ang_vel_publisher.publish(ang_vel_msg);
    */


    msg_out.data = x;
    x_publisher.publish(msg_out);
    msg_out.data = y;
    y_publisher.publish(msg_out);
    msg_out.data = phi;
    phi_publisher.publish(msg_out);


    angVel_out.data = left_angV;
    angVLeft_publisher.publish(angVel_out);
    angVel_out.data = right_angV;
    angVRight_publisher.publish(angVel_out);


    prev_right_count = right_count;
    prev_left_count = left_count;

    ros::spinOnce();
    loop_rate.sleep();
  }


	return 0;
}
