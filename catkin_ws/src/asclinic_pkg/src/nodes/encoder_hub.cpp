#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/Int32.h"
#include "std_msgs/UInt16.h"
#include "std_msgs/Float64.h"

#include <gpiod.h>


   int right_count = 0;
   int left_count = 0;

   int left_A_val = -1;
   int left_B_val = -1;
   int right_A_val = -1;
   int right_B_val = -1;

   int x = 0;
   int y = 0;
   int phi = 0;

   std_msgs::Int32 msg_out;


  ros::Publisher pub_count_left;
  ros::Publisher pub_count_right;


void left_A_change(const std_msgs::Int32& msg)
{
  left_A_val = msg.data;
  if ((left_B_val == 0 && left_A_val == 1) || (left_A_val == 0 && left_B_val == 1)) {
    left_count++;
  }
  else {
  left_count--;
  }
  msg_out.data = left_count;
  pub_count_left.publish(msg_out);
  ros::spinOnce();
}

void left_B_change(const std_msgs::Int32& msg)
{
  left_B_val = msg.data;
	if ((left_B_val == 1 && left_A_val == 1) || (left_A_val == 0 && left_B_val == 0)) {
	left_count++;
  }
  else {
	left_count--;
  }
  msg_out.data = left_count;
  pub_count_left.publish(msg_out);
  ros::spinOnce();
}

void right_A_change(const std_msgs::Int32& msg)
{
  right_A_val = msg.data;
  if ((right_B_val == 0 && right_A_val == 1) || (right_A_val == 0 && right_B_val == 1)) {
    right_count++;
  }
  else {
  right_count--;
  }
  msg_out.data = right_count;
  pub_count_right.publish(msg_out);
  ros::spinOnce();
}

void right_B_change(const std_msgs::Int32& msg)
{
  right_B_val = msg.data;
	if ((right_B_val == 1 && right_A_val == 1) || (right_A_val == 0 && right_B_val == 0)) {
	right_count++;
  }
  else {
	right_count--;
  }
  msg_out.data = right_count;
  pub_count_right.publish(msg_out);
  ros::spinOnce();
}

int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "encoder_hub");
	ros::NodeHandle nodeHandle("/encoder_hub");

	ROS_INFO_STREAM("[ENCODER HUB] Initialised.");

	pub_count_left = nodeHandle.advertise<std_msgs::Int32>("left_count", 10, false);
 	pub_count_right = nodeHandle.advertise<std_msgs::Int32>("right_count", 10, false);

 	// Initialise a subscriber
  ros::Subscriber subscr_left_channelA = nodeHandle.subscribe("/LeftA/value", 1, left_A_change);
  ros::Subscriber subscr_left_channelB = nodeHandle.subscribe("/LeftB/value", 1, left_B_change);
  ros::Subscriber subscr_right_channelA = nodeHandle.subscribe("/RightA/value", 1, right_A_change);
  ros::Subscriber subscr_right_channelB = nodeHandle.subscribe("/RightB/value", 1, right_B_change);

  ros::spin();

	return 0;
}
