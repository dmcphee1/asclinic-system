
#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/Int32.h"
#include "std_msgs/UInt16.h"
#include "std_msgs/Float64.h"

#include <gpiod.h>



// Respond to subscriber receiving a message
/*void templateSubscriberCallback(const std_msgs::Float64& msg)
{
	ROS_INFO_STREAM("[TIME NODE.] Message receieved with data = " << msg.data);
}*/

int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "time_node");
	ros::NodeHandle nodeHandle("/time_node");
	// Initialise a publisher
	//ros::Publisher gpio_event_publisher = nodeHandle.advertise<std_msgs::Int32>("gpio_event", 10, false);

	//OUR OWN PUBLISHING TEST
	//ros::Publisher test_publisher = nodeHandle.advertise<std_msgs::Int32>("template_namespace_for_launch_group/template_i2c_internal/set_motor_duty_cycle", 10, false);
  ros::Publisher test_publisher = nodeHandle.advertise<std_msgs::Float64>("/time_node/sim_time", 10, false);

	// Initialise a subscriber
	// > Note that the subscriber is included only for the purpose
	//   of demonstrating this template node running stand-alone
  //ros::Subscriber test_subscriber = nodeHandle.subscribe("/time_node/sim_time", 1, templateSubscriberCallback);




	// Initialise variables used for computing the time
	// between events on the GPIO line
	long int prev_tv_nsec = -1;
	bool prev_time_isValid = false;
  float this_diff_sec = -1;

  ros::Time begin = ros::Time::now();

	// Enter a loop that continues while ROS is still running
	while (ros::ok())
	{
    ros::Time current = ros::Time::now();
    ros::Duration time_difference = current - begin;
    //ROS_INFO_STREAM("durop = " << time_difference);

    // Publish a message
    std_msgs::Float64 msg;
	  msg.data = time_difference.toSec();
    test_publisher.publish(msg);
    // Spin once so that this node can service the any
    // callbacks that this node has queued.
    // > This is required so that message is actually
    //   published.
    ros::spinOnce();


  	} // END OF: "while (ros::ok())"



  	return 0;
}
